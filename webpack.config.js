const path = require('path');

const babelLoader = {
  loader: 'babel-loader',
  options: {
    // Disable reading babel configuration
    babelrc: false,
    configFile: false,

    // The configuration for compilation
    presets: [
      [
        '@babel/preset-env',
        {
          useBuiltIns: 'usage',
          corejs: 3
        }
      ],
      '@babel/preset-react',
      '@babel/preset-flow',
      "@babel/preset-typescript"
    ],
    plugins: [
      'react-native-web',
      'react-native-paper/babel',
      '@babel/plugin-proposal-class-properties',
      '@babel/plugin-proposal-object-rest-spread'
    ],
  },
}

module.exports = {
  mode: 'development',

  // Path to the entry file, change it according to the path you have
  entry: path.join(__dirname, 'src', 'index.js'),

  // Path for the output files
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'app.bundle.js',
  },

  // Enable source map support
  devtool: 'source-map',

  // Loaders and resolver config
  module: {
    rules: [
      {
        test: /\.(jpg|png|woff|woff2|eot|ttf|svg)$/,
        type: 'asset/resource'
      },
      {
        include: /(@material\/material-color-utilities)[/\\]/,
        resolve: {
          fullySpecified: false
        },
        test: /\.js$/,
        use: babelLoader
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules[/\\]/,
        use: babelLoader,
      },
    ],
  },
  resolve: {
    extensions: [
			'.js',
			'.jsx'
		],
    alias: {
      'react-native$': require.resolve('react-native-web'),
      'react-native-vector-icons': 'react-native-vector-icons/dist'
    }
  },

  // Development server config
  devServer: {
    static: [path.join(__dirname, 'public')],
    historyApiFallback: true,
  },
};