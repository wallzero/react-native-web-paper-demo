import {
	AppRegistry,
	Platform
	// eslint-disable-next-line import/no-deprecated,node/no-extraneous-import
} from 'react-native';
import App from './App';
import appJson from './app.json';

AppRegistry.registerComponent(
	appJson.name,
	() => {
		return App;
	}
);

if (Platform.OS === 'web') {
	AppRegistry.runApplication(
		appJson.name,
		{
			rootTag: document.querySelector('#app')
		}
	);
}
