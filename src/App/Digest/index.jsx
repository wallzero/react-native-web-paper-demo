import React from 'react';
import {
	Button
	// eslint-disable-next-line import/no-deprecated
} from 'react-native-paper';

const Digest = () => {
	return (
		<Button
			mode='contained'
			onPress={() => console.log('Pressed')}
		>
			Hello, world!
		</Button>
	);
};

export default Digest;
