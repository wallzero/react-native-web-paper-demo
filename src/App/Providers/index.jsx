import React from 'react';
import Paper from './Paper';

const Providers = ({
	children
}) => {
	return (
		<Paper>
			{children}
		</Paper>
	);
};

export default Providers;
