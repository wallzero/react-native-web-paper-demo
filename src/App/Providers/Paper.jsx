import React from 'react';
import {
	Provider as PaperProvider
} from 'react-native-paper';

const Paper = ({
	children
}) => {
	return (
		<PaperProvider>
			{children}
		</PaperProvider>
	);
};

export default Paper;
