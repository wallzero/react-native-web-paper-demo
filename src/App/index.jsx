import React, {
	StrictMode,
	Suspense
} from 'react';
import {
	Text
} from 'react-native';
import Lazy from './Lazy';

const App = () => {
	return (
		<StrictMode>
			<Suspense
				fallback={(
					<Text>
						Loading...
					</Text>
				)}
			>
				<Lazy />
			</Suspense>
		</StrictMode>
	);
};

export default App;
