import {
	lazy
} from 'react';

const Lazy = lazy(
	() => {
		return import(

			/* webpackChunkName: "Providers" */
			/* webpackPrefetch: true */
			'./App'
		);
	}
);

export default Lazy;
