import React from 'react';
import {
	SafeAreaView
} from 'react-native';
import Digest from './Digest';
import Providers from './Providers';

const App = () => {
	return (
		<SafeAreaView>
			<Providers>
				<Digest />
			</Providers>
		</SafeAreaView>
	);
};

export default App;
